SFOT projekt nr 1, zespół nr 5 <!-- omit in toc -->
==============================

- [Polecenie](#polecenie)
- [Założenia](#założenia)
- [Obliczenie zapotrzebowania na energię elektryczną](#obliczenie-zapotrzebowania-na-energię-elektryczną)
- [Schemat blokowy systemu](#schemat-blokowy-systemu)
- [Dobór mocy szczytowej instalacji według założeń projektu](#dobór-mocy-szczytowej-instalacji-według-założeń-projektu)
  - [Średnia dzienna ilość godzin słonecznych](#średnia-dzienna-ilość-godzin-słonecznych)
  - [Uwzględnienie temperatury modułu innej niż dla STC](#uwzględnienie-temperatury-modułu-innej-niż-dla-stc)
  - [Straty okablowania](#straty-okablowania)
  - [Straty konwersji energii](#straty-konwersji-energii)
  - [Straty niedopasowania](#straty-niedopasowania)
  - [Straty związane ze starzeniem się modułów](#straty-związane-ze-starzeniem-się-modułów)
  - [Obliczanie mocy nominalnej systemu](#obliczanie-mocy-nominalnej-systemu)
- [Dobór elementów instalacji](#dobór-elementów-instalacji)
  - [Moduły](#moduły)
  - [Falowniki i konfiguracja modułów](#falowniki-i-konfiguracja-modułów)
  - [Akumulatory i inwerter akumulatorów](#akumulatory-i-inwerter-akumulatorów)
  - [Schemat końcowy systemu](#schemat-końcowy-systemu)
- [Analiza kosztów](#analiza-kosztów)
- [Podsumowanie](#podsumowanie)
- [Bibliografia](#bibliografia)

## Polecenie

> Zaprojektuj system wolnostojący pozwalający na zasilenie 10 pomp ogrodowych zasilających uprawę roślin o wysokim zapotrzebowaniu na wodę. Rośliny te muszą być nawadniane codziennie przez 2h rano oraz 2h wieczorem. Podlewanie musi odbywać się w miesiącach kwiecień – wrzesień. System ma być zlokalizowany w Holandii w pobliżu miejscowości Lisse i uwzględniać możliwość wystąpienia 2 pochmurnych dni z rzędu.

<!---
Wymagania projektu oraz warunki zaliczenia zostały przedstawione w poprzedniej wiadomości. Koordynatorem Państwa projektu będzie:

mgr inż. Kinga Kondracka (kontakt mailowy w CC).

Bardzo proszę o zapoznanie się z wymaganiami projektu:

1. Obliczenie/oszacowanie zapotrzebowania na energię eklektyczną. 
2. Dobór mocy szczytowej instalacji według założeń projektu. 
3. Dobór elementów instalacji (moduły PV – rodzaj, technologia wykonania, liczba, powierzchnia; falowniki; liczniki; okablowanie) oraz obliczenie/oszacowanie ich kosztów. 
4. Ważnym elementem jest optymalizacja projektu pod względem kosztów.
5. Obliczenie uzysku energetycznego z instalacji z podziałem na miesiące/godziny w zależności od wymagań projektu. 
6. Jeśli wybrany software nie pozwala na ustawienie dokładnej lokalizacji zaznaczonej w projekcie, proszę wybrać możliwą najbliższą lokalizację lub taką o podobnych warunkach pogodowych.
7. Do każdego z projektów należy wykonać jego szczegółowy opis wraz z obliczeniami, wykresami oraz schematycznymi rysunkami.
8. Do każdego z projektów należy przygotować dokumentację „dla klienta”. 
9. Ocenie podlega zarówno wartość merytoryczna raportu oraz przejrzystość i estetyka dokumentacji. 
10. Wybór software’u zastosowanego do realizacji projektu jest dowolny (przykłady: PVsol, PVsyst)
11. W przypadku realizacji danego tematu przez więcej niż 1 zespół kluczowa będzie oryginalność zaproponowanych rozwiązań.
12. Maksymalna liczba punktów do zdobycia wynosi 16.
13. Czas na realizację projektu upływa 31 maja 2020. Po tym czasie maksymalna liczba punktów do zdobycia będzie malała o 2 punkty każdego dnia roboczego. Projekty przesłane po 10 czerwca 2020 nie będą oceniane.
14. Projekty proszę przesyłać drogą mailową na adres mailowy przydzielonego koordynatora.
-->

## Założenia

> + system wolnostojący, niepodłączony do sieci
> + system ma pracować od kwietnia do września, codziennie przez 2h rano i 2h wieczorem ze stałą mocą, czyli praca przez 4 godziny dziennie w czasie niskiego nasłonecznienia - konieczność magazynowania energii
> + 10 pomp ogrodowych o mocy 900W, zasilanie 230V AC - potrzebny falownik
> + lokalizacja: Lisse, Holandia
> + dowolność orientacji paneli słonecznych
> + możliwość wystąpienia dwóch pochmurnych dni z rzędu

## Obliczenie zapotrzebowania na energię elektryczną

> Pompy są włączane przez 4 godziny dziennie, zapotrzebowanie na energię we wszystkich miesiącach działania systemu jest takie same, zatem:

> $W = n_{pump} \times P_{pump} \times t_{work} = 10 \times 900W \times 4h = 36kWh/d$, gdzie
>
> + $W [kWh/d]$ - dzienne zapotrzebowanie na energię elektryczną
> + $n_{pump}$ - liczba pomp ogrodowych
> + $P_{pump} [W]$ - moc pompy ogrodowej
> + $t_{work} [h]$ - czas pracy pomp ogrodowych w ciągu dnia
>
> Dzienne zapotrzebowanie na energię elektryczną od kwietnia do września wynosi 36kWh, w pozostałych miesiącach 0kWh. 
>

## Schemat blokowy systemu

> ![image alt std_img_60](images/system_block_schematic_1.png)

## Dobór mocy szczytowej instalacji według założeń projektu

### Średnia dzienna ilość godzin słonecznych 

> Dla lokalizacji Lisse, Holadnia, korzystając z [bazy danych meteorologicznych PVGIS-5](https://re.jrc.ec.europa.eu/pvg_tools/en/tools.html?lat=&lon=&startyear=&endyear=&raddatabase=&angle=&browser=&outputformat=&userhorizon=&usehorizon=1&js=1&select_database_month=PVGIS-SARAH&mstartyear=2005&mendyear=2016&horirrad=1&optrad=1&selectrad=1&mangle=30) uzyskano informacje o miesięcznym nasłonecznieniu na powierzchnię modułów przy optymalnym ustawieniu w latach 2005 - 2016.
>
> ![image alt std_img_60](images/imonthly_irradiation_1.png)
>
> ![image alt std_img_80](images/imonthly_irradiation_2.png)

> Dla miesięcy o najmniejszym nasłonecznieniu w rozważanym zakresie, czyli dla kwietnia i września wybrano najmniejszą wartość (kwiecień i wrzesień mają taką samą liczbę dni), jest to wartość $100.11 kWh/m^2$ dla września 2007r. Na tej podstawie obliczono iloczyn parametrów $Z_1$ i $Z_2$:
>
> $Z_1 \times Z_2 = \frac{H_{month}}{n_{month} \times G_{STC}} = \frac{100.11 kWh/m^2}{30d \times 1kW/m^2} = 3.3h/d$, gdzie:
> + $Z_1$ - średnia dzienna ilość godzin słonecznych z płaszczyźnie powierzchni znormalizowana do STC [$h/d$]
> + $Z_2$ - współczynnik związany z odchyleniem od płaszczyzny poziomej
> + $H_{month}$ - miesięczne nasłonecznienie na płaszczyznę modułów dla optymalnego ustawienia [$kWh/m^2$]
> + $n_{month}$ - liczba dni w miesiącu
> + $G_{STC} = 1000\frac{W}{m^2}$ -  standardowe natężenie promieniowania w warunkach STC
>
> Optymalne ustawienie paneli to kierunek południowy, nachylenie do poziomu pomiędzy $30^\circ$ a $45^\circ$, sprawdzono, że w tym zakresie wartości nasłonecznienia zmieniają się nieznacznie.
>

### Uwzględnienie temperatury modułu innej niż dla STC

> Wartość współczynnika $Z_3$, zależy od nasłonecznienia, temperatury otoczenia i wentylacji modułów. Założono, że moduły będą wolnostojące, dzięki czemu będą miały dobrą wentylację. Na podstawie [1] wybrano wartość dla Berlina we wrześniu:  
> $Z_3 = 0.98$

> ![image alt std_img_80](images/Z3_factors.png)

### Straty okablowania

> Straty okablowania zależą od średnicy i długości zastosowanych przewodów oraz od napięcia w systemie, im większe napięcie przy danej mocy tym niższe straty przewodzenia. Dla uproszczenia z góry założono straty z okablowania na poziomie 5%, mając dalej na uwadze powyższe zależności. We współczynniku tym należy także wziąć pod uwagę straty w kablach doprowadzających zasilanie do pomp ogrodowych, gdzie napięcie zasilania za falownikiem jest narzucone, długość tych kabli może być znaczna. Współczynnik związany ze stratami okablowania:  
> $V_L = 1 - 0.05 = 0.95$

### Straty konwersji energii

> W baterii zachodzi konwersja energii elektrycznej na chemiczną i na odwrót. Podczas tego procesu powstają straty zależne od typu baterii oraz temperatury otoczenia. Należy tutaj też uwzględnić sprawność falownika. Dla uproszczenia na podstawie [1] przyjęto straty konwersji na poziomie 10%. Współczynnik strat konwersji:  
> $V_U = 1 - 0.1 = 0.9$

### Straty niedopasowania

> Straty niedopasowania wynikają z tego, że sterownik ładowania baterii nie pracuje dokładnie w punkcie MPP (punkt optymalnego obciążenia modułów). Dla sterowników MPPT, czyli takich, które kontrolują punkt optymalnego obciążenia straty te są niewielkie, na poziomie 2% (na podstawie [1]). Przyjęto, że w projekcie zostanie zastosowany sterownik MPPT. Współczynnik strat niedopasowania:  
> $V_A = 1 - 0.02 = 0.98$

### Straty związane ze starzeniem się modułów

> Założono czas pracy pola modułów równy 25 lat oraz spadek mocy wyjściowej po tym czasie do 80%. Współczynnik starzenia:
> $V_S = 0.8$ 

### Obliczanie mocy nominalnej systemu

> $P_{PV} = \frac{W}{(Z_1 \times Z_2) \times Z_3 \times V_L \times V_U \times V_A \times V_S} = \frac{36kWh/d}{(3.3h/d) \times 0.98 \times 0.9 \times 0.98 \times 0.8} = 15.75kW$, gdzie:
> + $P_{PV} [kW]$ - moc nominalna modułów
> + $W [kWh/d]$ - dzienne zapotrzebowanie na energię elektryczną
> + $Z_1$ - średnia dzienna ilość godzin słonecznych z płaszczyźnie powierzchni znormalizowana do STC [$h/d$]
> + $Z_2$ - współczynnik związany z odchyleniem od płaszczyzny poziomej
> + $Z_3$ - współczynnik korekcji odchyłki temperatury modułów od warunków STC
> + $V_L$ - współczynnik strat okablowania
> + $V_U$ - współczynnik strat konwersji
> + $V_A$ - współczynnik strat niedopasowania
> + $V_S$ - współczynnik strat związany ze starzeniem się modułów

> System jest projektowany tak, aby działał poprawnie dla miesięcy o najmniejszym nasłonecznieniu w okresie wegetacji roślin, ale ze względu na znaczną różnicę w nasłonecznieniu między kwietniem/wrześniem a miesiącami letnimi, w miesiącach tych będzie występowała nadwyżka energii w instalacji. Również w miesiącach zimowych, gdy instalacja nie jest używana, energia może być produkowana. Warto poinformować o tym klienta, być może zdecyduje się wykorzystać w jakiś sposób nadwyżkę energii, tym bardziej że będzie dysponował powszechnie używanym napięciem 230V AC. Być może zdecyduje się na instalację pompy głębinowej, która pompowałaby wodę do zbiornika, jeśli byłoby to bardziej opłacalne niż korzystanie z wody wodociągowej.  

## Dobór elementów instalacji

### Moduły

> W projekcie brak jest ograniczeń na powierzchnię modułów, więc sprawność modułów jest mniej istotna, ważny jest koszt jednostkowy za 1kW mocy modułów. O cenie modułu decyduje nie tylko technologia w której został wykonany, ale także dostępność towaru w danym miejscu, dlatego przy wyborze modułów kierowano się nie technologią, a ceną/kW na jednym z popularnych serwisów ogłoszeniowych. Wybrano moduł:

> ![image alt std_img_80](images/module_model.png)

> Moc nominalna wybranego modułu wynosi $P_{mod} = 315W$, zatem wymagana liczba modułów w systemie:
> $n_{mod} = \frac{P_{PV}}{P_{mod}} = \frac{15.75kW}{315W} = 50$

> Napięcie w punkcie MPP dla STC:
> $V_{mod} = 33V$

> Prąd w punkcie MPP dla STC:
> $I_{mod} = 9.55A$

### Falowniki i konfiguracja modułów

> Wybrano falowniki z kontrolą optymalnego obciążenia (MPPT). Falowniki powinny być w stanie dostarczyć moc nominalną pola modułów do akumulatorów (mogą mieć nieco mniejszą moc, ponieważ przez zdecydowaną większość czasu moduły nie pracują z mocą nominalną). Powinny mieć odpowiednią liczbę wejść, oraz zakresy napięć i prądu pracy dopasowane do konfiguracji modułów. Do wybrania odpowiedniej konfiguracji użyto oprogramowania symulacyjnego. Uzyskane wyniki:
> + 3 falowniki z dwoma wejściami MPPT
> + 6 łańcuchów: 4x8 modułów, 2x9 modułów

> ![image alt std_img_80](images/falowniki_check_1.png)
> ![image alt std_img_80](images/falowniki_check_1_1.png)

> ![image alt std_img_80](images/falowniki_check_2.png)
> ![image alt std_img_80](images/falowniki_check_2_1.png)

### Akumulatory i inwerter akumulatorów

> Projekt musi uwzględniać możliwość wystąpienia dwóch pochmurnych dni z rzędu. Oznacza to, że po słonecznym dniu, podczas którego akumulatory zostają w pełni naładowane następuje cykl podlewania 2h wieczorem i 2h rano następnego dnia. Podczas dwóch następnych pochmurnych dni cykl się powtarza dwa razy. Oznacza to, że trzeba przyjąć $A = 3$ dni pracy autonomicznej. Dla uproszczenia założono, że podczas pochmurnych dni akumulatory się nie ładują, choć w rzeczywistości zawsze częściowo się ładują (poza przypadkiem awarii generatora PV). Projekt jest więc trochę na wyrost względem potrzeb klienta, ale jest bezpieczny w przypadku gdyby nastąpiła awaria generatora PV, gdyż daje czas na serwis, a 3 dni na serwis to i tak może być niewiele.

> Aby zachować maksymalną żywotność akumulatorów stan naładowania nie może nigdy spaść poniżej 30%. Zapewnia to inwerter akumulatorów, ale dla projektu oznacza to, że obliczoną potrzebną pojemność akumulatora należy podzielić przez współczynnik $B = 0.7$.

> Wstępnie przyjęto, że zostaną zastosowane akumulatory o napięciu pracy $U_n = 48V$. Na etapie optymalizacji kosztów systemu ten wybór należy zweryfikować i tak dopasować napięcie akumulatorów i sterownik ładowania, aby zminimalizować koszty systemu, choć na podstawie [1, str. 285], w przypadku akumulatorów koszt jest proporcjonalny do gromadzonej energii w Wh, a nie do pojemności wyrażonej w Ah, więc pod tym względem napięcie akumulatorów ma drugorzędne znaczenie. 

> Wymagana pojemność akumulatorów o napięciu pracy $U_n = 48V$:  
> 
> $C_n = \frac{W \times A}{B \times U_n} = \frac{36kWh/d \times 3d}{0.7 \times 48V} \approx 3225Ah$, gdzie:
> + $W [kWh/d]$ - dzienne zapotrzebowanie na energię elektryczną
> + $A$ - liczba dni autonomicznych
> + $B$ - współczynnik związany z maksymalnym dopuszczalnym rozładowaniem akumulatorów
> + $U_n$ - napięcie pracy akumulatorów

> Do wyboru inwerterów i akumulatorów użyto oprogramowania symulacyjnego. Poniżej poadano wyniki:
>
> ![image alt std_img_80](images/battery_inverter.png)
>
> Energia gromadzona przez akumulatory musiała być nieco większa niż obliczona wyżej, prawdopodobnie wynika to z tego, że oprogramowanie oblicza nieco inaczej straty w systemie.

### Schemat końcowy systemu

> ![image alt std_img_80](images/final_system_schematic.png) 

## Analiza kosztów

> ![image alt std_img_100](images/cost_total.png) 

> Dominuje koszt akumulatorów. Rozwiązanie systemu wyspowego bez zasilania pomocniczego wydaje się być rozwiązaniem nieoptymalnym. Warto zaproponować klientowi rozwiązanie on-grid lub dodatkowe zasilanie z agregatu prądotwórczego lub turbiny wiatrowej.

## Podsumowanie

> W pliku `./simulation/PSYL PRO1 Z5.pvprj` znajduje się podsumowanie systemu. Niestety oprogramowanie PV sol w wersji trial nie pozwala wygenerować raportu. 

## Bibliografia

> + [1] Deutsche Gesellshaft Für Sonnenenergie - Planning and Installing Photovoltaic Systems : A Guide for Installers, Architects and Engineers



